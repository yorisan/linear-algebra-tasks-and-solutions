#!/bin/sh

if [[ "$1" =~ "main.tex" ]]; then
  file=$(readlink -f "$1")
else
  file="$(dirname $(readlink -f "$1"))/../main.tex"
fi
dir=$(dirname "$file")

cd "$dir" || exit

textype() { \
	command="pdflatex"
	( sed 5q "main.tex" | grep -i -q 'xelatex' ) && command="xelatex"
	$command --output-directory="$dir" "main.tex"
	}

textype "$file"
