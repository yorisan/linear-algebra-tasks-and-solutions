from sympy import *
import re
init_printing(use_unicode=True)

def pe():
    print(r"\begin{equation*}")
    print(r"\setlength{\jot}{10pt}")
    print(r"\begin{aligned}")
    print(r"&")

def pee():
    print(r"\end{aligned}")
    print(r"\end{equation*}")


def sim():
    print("\sim")

def pmat(a):
    a = latex(a)
    a = re.sub(r"\\left\[", r"", a)
    a = re.sub(r"\\right\]", r"", a)
    a = re.sub(r"\\\\", r" \\\\\n", a)
    a = re.sub(r"(\\begin{\w+})", r"\1\n", a)
    a = re.sub(r"(\\end{\w+})", r"\n\1", a)
    a = re.sub(r"matrix", r"bmatrix", a)
    print(a)

a = Matrix([
        [-3, 2, 0, 1, 4],
        [-1, 5, 2, 3, 5],
        [6, -12, 3, -7, -8],
        [-3, 7, 9, 4, 15],
        ])

pe()
pmat(a)
a[0,:] -= a[3,:]
a[2,:] += 2*a[3,:]
a[3,:] -= 3*a[1,:]
sim()
pmat(a)
a[1,:] = -a[1,:]
sim()
pee()

pe()
sim()
pmat(a)
a[1,:] -= a[0,:]
a[3,:] -= a[0,:]
sim()
pmat(a)
sim()
pee()

pe()
sim()
a[0,:] += a[2,:]
pmat(a)
a[3,:] *= 0
sim()
pmat(a)
sim()
pee()

pe()
sim()
a[0,:] += a[2,:]
pmat(a)
a[2,:] += 2*a[0,:]
a[0,:] *= -1
sim()
pmat(a)
print(".")
pee()

print(r"$\rank A = 3$")
